import os
import mysql.connector
from mysql.connector import Error

host = os.environ["HOST"]
port = os.environ["PORT"]
database = os.environ["DATABASE"]
user = os.environ["USER"]
password = os.environ["PASSWORD"]

def connectionMySQL():
    connection = None
    try:
        connection = mysql.connector.connect(host=host,
                                            port=port,
                                            database=database,
                                            user=user,
                                            password=password)
        if connection.is_connected():
            db_Info = connection.get_server_info()
            print("Connected to MySQL Server version ", db_Info)

    except Error as e:
        print("Error while connecting to MySQL", e)

    return connection

def execute_query(connection, query):
    cursor = connection.cursor()
    try:
        cursor.execute(query)
        connection.commit()
        print("Query successful")
    except Error as err:
        print(f"Error: '{err}'")

def select_query(connection, query):
    cursor = connection.cursor()
    result = None
    try:
        cursor.execute(query)
        result = cursor.fetchall()
        return result
    except Error as err:
        print(f"Error: '{err}'")
