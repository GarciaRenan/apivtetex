import requests

class Vtex_API:

    def __init__(self):
        self._url = 'https://labellamafia.vtexcommercestable.com.br'
        self._payload = {}
        self._headers = {
                            'X-VTEX-API-AppKey': 'vtexappkey-labellamafia-YSZKNL',
                            'X-VTEX-API-AppToken': 'PZCQTWBSPVJCKZWJHICSOTJHTSMIFUCFTMFNLCDCBLFGBZQPLPTVCXHWPVXBKTNUTDIXTIYEFZQCOCGTBXWPAHDWFJYQFBSIYKNXRTVDQOQCGEINPDAMWXTLAMSKKIZD',
                            'Cookie': 'janus_sid=1a09bc8e-8f43-4597-89a4-d2029ed63f0e'
                        }

    def order_vtex(self, orderId):
        url = f"{self._url}/api/oms/pvt/orders/{orderId}"
        return requests.request("GET", url, headers=self._headers, data=self._payload).json()

    def list_order(self, dt_ini, dt_fim, page):
        url = f"{self._url}/api/oms/pvt/orders?f_hasInputInvoice=false&per_page=30&f_creationDate=creationDate%3A%5B{dt_ini}.000Z%20TO%20{dt_fim}.999Z%5D&page={page}"
        return requests.request("GET", url, headers=self._headers, data=self._payload).json()

    def user_order(self, email):
        url = f"{self._url}/api/oms/user/orders/?clientEmail={email}"
        return requests.request("GET", url, headers=self._headers, data=self._payload).json()