from Vtex_API import Vtex_API
from Orders import Orders
import datetime             # fazer contas com datas
import pandas               # trabalhar com os datasets


# seta a data com um dia anterior ao ser iniciada a api
dt_ini = ''
dt_fim = datetime.date(2021, 7, 17)

vtex_api = Vtex_API()

for r in range(999):
    # inicialização das datas a serem buscadas
    dt_ini = dt_fim + datetime.timedelta(days=1)
    dt_fim = dt_ini + datetime.timedelta(days=3)
    print('ini', dt_ini)
    print('fim', dt_fim)

    # inicialização da lista
    resultado = []

    # somente para buscar a quantidade de páginas na API ListOrder
    res_pages = vtex_api.list_order(dt_ini, dt_fim, '1', )
    print(res_pages)
    res_pages = res_pages['paging']['pages']

    if res_pages == 0:
        break

    # Iatera por todas as páginas
    for i in range(res_pages):
        # executa a api para cada página
        res = vtex_api.list_order(dt_ini, dt_fim, i+1)
        print('*********')
        print('page', i+1)

        # Loop para pegar os resultados por página
        for idx, oid in enumerate(res['list']):

            # if idx > 3:
            #     break
            
            # printa o index atual para acompanhamento
            print('item', idx+1)

            # busca o resultado de cada orderId
            res2 = vtex_api.order_vtex(oid['orderId'])

            # Seta as variáveis com os resultados
            orderId = oid['orderId']
            creationDate = res2['creationDate']
            creationDate = creationDate[:19]
            creationDate = datetime.datetime.strptime(creationDate, '%Y-%m-%dT%H:%M:%S')
            creationDate = creationDate.strftime('%d/%m/%Y %H:%M:%S')

            host = res2['hostname']
            try:
                nameCupom = res2['ratesAndBenefitsData']['rateAndBenefitsIdentifiers'][1]['name']
            except (IndexError, KeyError) as e:
                nameCupom = ''
            try:
                cupomAplicado = res2['ratesAndBenefitsData']['rateAndBenefitsIdentifiers'][1]['matchedParameters']['couponCode@Marketing']
            except (IndexError, KeyError) as e:
                cupomAplicado = ''
            try:
                if res2['paymentData']['transactions'][0]['payments'][0]['group'] == 'null':
                    paymentType = res2['paymentData']['transactions'][0]['payments'][0]['paymentSystemName']
                else:
                    paymentType = res2['paymentData']['transactions'][0]['payments'][0]['group']
            except (IndexError, KeyError) as e:
                paymentType = ''
            total_Itens = res2['totals'][0]['value']
            total_Descontos = res2['totals'][1]['value']
            total_Frete = res2['totals'][2]['value']
            total_Pedido = res2['value']
            try:
                utmSource = res2['marketingData']['utmSource']
            except TypeError:
                utmSource = ''
            try:
                utmPartner = res2['marketingData']['utmPartner']
            except TypeError:
                utmPartner = ''
            try:
                utmMedium = res2['marketingData']['utmMedium']
            except TypeError:
                utmMedium = ''
            try:
                utmCampaign = res2['marketingData']['utmCampaign']
            except TypeError:
                utmCampaign = ''

            # Seta as variáveis na classe
            r = Orders(
                        orderId,
                        creationDate,
                        host,
                        nameCupom,
                        cupomAplicado,
                        paymentType,
                        total_Itens,
                        total_Descontos,
                        total_Frete,
                        total_Pedido,
                        utmSource,
                        utmPartner,
                        utmMedium,
                        utmCampaign
                        )

            # insere o objeto no array
            resultado.append(r)

    #  EXPORTAR PARA CSV
    # seta as colunas do data frame
    df = pandas.DataFrame(columns=[
                                    'ExtractionDate',
                                    'OrderId',
                                    'CreationDate',
                                    'Host',
                                    'nameCupom',
                                    'CupomAplicado',
                                    'PaymentType',
                                    'Total_Itens',
                                    'Total_Descontos',
                                    'Total_Frete',
                                    'Total_Pedido',
                                    'UtmSource',
                                    'UtmPartner',
                                    'utmMedium',
                                    'UtmCampaign'
                                ])
    # itera no resultado para atribuir as tuplas em cada linha do dataframe
    for i in range(len(resultado)):
        df.loc[len(df)] = [
                            resultado[i].extractionDate,
                            resultado[i].orderId[0],
                            resultado[i].creationDate[0],
                            resultado[i].host[0],
                            resultado[i].nameCupom[0],
                            resultado[i].cupomAplicado[0],
                            resultado[i].paymentType[0],
                            resultado[i].total_Itens[0],
                            resultado[i].total_Descontos[0],
                            resultado[i].total_Frete[0],
                            resultado[i].total_Pedido[0],
                            resultado[i].utmSource[0],
                            resultado[i].utmPartner[0],
                            resultado[i].utmMedium[0],
                            resultado[i].utmCampaign
                        ]
    # salva o resultado do dataframe em um arquivo csv
    df.to_csv(f"order_Vtex{dt_ini}.csv",index=False)

    print('Arquivo criado: order_Vtex')
