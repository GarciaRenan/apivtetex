import os
import locale
import datetime             # fazer contas com datas
import pandas               # trabalhar com os datasets
from sqlalchemy import create_engine
from sqlalchemy.engine import URL

import pytz                 # biblioteca para alterar o timezone
from Vtex_API import Vtex_API
from Orders import Orders

locale.setlocale(locale.LC_ALL, 'pt_BR.UTF-8')

connect_url = URL.create(
    'mysql+pymysql',
    username=os.environ["USER"],
    password=os.environ["PASSWORD"],
    host=os.environ["HOST"],
    port=os.environ["PORT"],
    database=os.environ["DATABASE"])
engine = create_engine(connect_url)

tsIni = datetime.datetime.now()
brtz = pytz.timezone('Brazil/East')
utc = pytz.utc

# seta a data com um dia anterior ao ser iniciada a api
dt_ini = datetime.datetime(2021, 1, 1, 0, 0, 0) - datetime.timedelta(hours=4)
dt = dt_ini.date()
x = 0

vtex_api = Vtex_API()

while dt_ini < datetime.datetime(2022, 1, 1, 0, 0, 0):

    print('************************************')
    # inicialização das datas a serem buscadas

    # busca de 4 em 4 horas por serem muitos dados
    dt_ini = dt_ini + datetime.timedelta(hours=6)
    dt_fim = dt_ini + datetime.timedelta(hours=6)
    dt_i = dt_ini.strftime("%Y-%m-%dT%H:%M:%S")
    dt_f = dt_fim.strftime("%Y-%m-%dT%H:%M:%S")
    print(dt_i, dt_f)

    # contagem de vezes que passou pelo mesmo dia para nomer o arquivo
    dt1 = dt_ini.date()
    if dt1 != dt:
        dt = dt1
        x = 0
    else:
        x += 1

    # inicialização da lista
    resultado = []

    # somente para buscar a quantidade de páginas na API ListOrder
    # dt ini está nos dois parâmetros para pegar as informações por dia
    res_pages = vtex_api.list_order(dt_i, dt_f, '1')

    res_pages = res_pages['paging']['pages']

    # caso não retorne resultado sai do while
    if res_pages == 0:
        next

    # Iatera por todas as páginas
    for i in range(res_pages):

        # executa a api para cada página
        res = vtex_api.list_order(dt_i, dt_f, i+1)

        print('page', f'{i+1} of {res_pages} - {dt_ini}')

        # Loop para pegar os resultados por página
        for idx, oid in enumerate(res['list']):

            # if idx > 0:
            #     break

            # printa o index atual para acompanhamento
            print(
                f"Item {idx+1} de {len(res['list'])} - orderid: {oid['orderId']}")

            # busca o resultado de cada orderId
            compra = vtex_api.order_vtex(oid['orderId'])
            # compra = vtex_api.order_vtex('v2917437lbll-01')

            # Seta as variáveis com os resultados
            orderId = oid['orderId']
            origin = compra['origin']
            affiliateId = compra['affiliateId']
            salesChannel = compra['salesChannel']
            status = compra['status']
            statusDescription = compra['statusDescription']

            creationDateTime = compra['creationDate']
            # # substring da data e hora
            # creationDateTime = creationDateTime[:19]
            # creationDateTime = datetime.datetime.strptime(
            #     creationDateTime, '%Y-%m-%dT%H:%M:%S')  # converter para datetime
            # # alterar tzinfo de None para utc
            # creationDateTime = creationDateTime.replace(tzinfo=pytz.utc)
            # # alterar para o timezone do Brasil
            # creationDateTime = creationDateTime.astimezone(brtz)
            # creationDateTime = creationDateTime.strftime(
            #     '%d/%m/%Y %H:%M:%S')   # converter para texto novamente

            lastChangeDateTime = compra['lastChange']
            # # substring da data e hora
            # lastChangeDateTime = lastChangeDateTime[:19]
            # lastChangeDateTime = datetime.datetime.strptime(
            #     lastChangeDateTime, '%Y-%m-%dT%H:%M:%S')  # converter para datetime
            # # alterar tzinfo de None para utc
            # lastChangeDateTime = lastChangeDateTime.replace(tzinfo=pytz.utc)
            # # alterar para o timezone do Brasil
            # lastChangeDateTime = lastChangeDateTime.astimezone(brtz)
            # lastChangeDateTime = lastChangeDateTime.strftime(
            #     '%d/%m/%Y %H:%M:%S')

            authorizedDateTime = compra['authorizedDate']
            # if(compra['authorizedDate'] != None):
            #     # substring da data e hora
            #     authorizedDateTime = authorizedDateTime[:19]
            #     authorizedDateTime = datetime.datetime.strptime(
            #         authorizedDateTime, '%Y-%m-%dT%H:%M:%S')  # converter para datetime
            #     # alterar tzinfo de None para utc
            #     authorizedDateTime = authorizedDateTime.replace(
            #         tzinfo=pytz.utc)
            #     # alterar para o timezone do Brasil
            #     authorizedDateTime = authorizedDateTime.astimezone(brtz)
            #     authorizedDateTime = authorizedDateTime.strftime(
            #         '%d/%m/%Y %H:%M:%S')

            invoicedDateTime = compra['invoicedDate']
            # if(compra['invoicedDate'] != None):
            #     # substring da data e hora
            #     invoicedDateTime = invoicedDateTime[:19]
            #     invoicedDateTime = datetime.datetime.strptime(
            #         invoicedDateTime, '%Y-%m-%dT%H:%M:%S')  # converter para datetime
            #     # alterar tzinfo de None para utc
            #     invoicedDateTime = invoicedDateTime.replace(tzinfo=pytz.utc)
            #     # alterar para o timezone do Brasil
            #     invoicedDateTime = invoicedDateTime.astimezone(brtz)
            #     invoicedDateTime = invoicedDateTime.strftime(
            #         '%d/%m/%Y %H:%M:%S')

            # totals
            total_Itens = compra['totals'][0]['value'] / 100
            total_Discounts = compra['totals'][1]['value'] / 100
            total_Shipping = compra['totals'][2]['value'] / 100
            total_Tax = compra['totals'][3]['value'] / 100

            # items
            items_compra = compra['items']
            item_compra = []  # tmp list

            # passa por cada item da compra
            for item in items_compra:
                item_detail = {}  # tmp dict
                item_detail['items_id'] = item['id']
                item_detail['items_quantity'] = item['quantity']
                item_detail['items_price'] = item['price'] / 100
                item_detail['items_listPrice'] = item['listPrice'] / 100

                try:
                    changesAttachment = compra['changesAttachment']
                    if(len(changesAttachment) > 0):
                        for changes in changesAttachment['changesData']:
                            if(len(changes['itemsAdded']) > 0):
                                for addedItem in changes['itemsAdded']:
                                    if(addedItem['id'] == item_detail['items_id']):
                                        item_detail['changesAttachment_itemsAdded_reason'] = changes['reason']
                                        item_detail['changesAttachment_itemsAdded_price'] = addedItem['price']/100
                                        item_detail['changesAttachment_itemsAdded_quantity'] = addedItem['quantity']
                                    else:
                                        item_detail['changesAttachment_itemsAdded_reason'] = ''
                                        item_detail['changesAttachment_itemsAdded_price'] = 0
                                        item_detail['changesAttachment_itemsAdded_quantity'] = 0
                            else:
                                item_detail['changesAttachment_itemsAdded_reason'] = ''
                                item_detail['changesAttachment_itemsAdded_price'] = 0
                                item_detail['changesAttachment_itemsAdded_quantity'] = 0

                            if(len(changes['itemsRemoved']) > 0):
                                for removedItem in changes['itemsRemoved']:
                                    if(removedItem['id'] == item_detail['items_id']):
                                        item_detail['changesAttachment_itemsRemoved_reason'] = changes['reason']
                                        item_detail['changesAttachment_itemsRemoved_price'] = removedItem['price']/100
                                        item_detail['changesAttachment_itemsRemoved_quantity'] = removedItem['quantity']
                                    else:
                                        item_detail['changesAttachment_itemsRemoved_reason'] = ''
                                        item_detail['changesAttachment_itemsRemoved_price'] = 0
                                        item_detail['changesAttachment_itemsRemoved_quantity'] = 0
                            else:
                                item_detail['changesAttachment_itemsAdded_reason'] = ''
                                item_detail['changesAttachment_itemsAdded_price'] = 0
                                item_detail['changesAttachment_itemsAdded_quantity'] = 0

                except (IndexError, TypeError, KeyError) as e:
                    item_detail['changesAttachment_itemsAdded_reason'] = ''
                    item_detail['changesAttachment_itemsAdded_price'] = ''
                    item_detail['changesAttachment_itemsAdded_quantity'] = ''
                    item_detail['changesAttachment_itemsRemoved_reason'] = ''
                    item_detail['changesAttachment_itemsRemoved_price'] = ''
                    item_detail['changesAttachment_itemsRemoved_quantity'] = ''

                item_price_tag = item['priceTags']
                items_priceTags_value = 0
                for price_tag in item_price_tag:
                    if(price_tag['name'] == 'DISCOUNT@MARKETPLACE'):
                        # caso tenha mais de um item adiciona "," e o próximo item
                        items_priceTags_value = items_priceTags_value + (price_tag['value']/100) if items_priceTags_value != 0 else (
                            price_tag['value']/100)
                item_detail['items_priceTags_value'] = items_priceTags_value

                item_detail['items_additionalInfo_brandName'] = item['additionalInfo']['brandName']
                item_compra.append(item_detail)

            # clientProfileData
            clientProfileData = compra['clientProfileData']
            clientProfileData_firstName = clientProfileData['firstName']
            clientProfileData_phone = clientProfileData['phone']
            clientProfileData_email = clientProfileData['email']

            # marketingData
            try:
                marketingData_utmSource = compra['marketingData']['utmSource']
            except (IndexError, TypeError, KeyError) as e:
                marketingData_utmSource = ''
            try:
                marketingData_utmMedium = compra['marketingData']['utmMedium']
            except (IndexError, TypeError, KeyError) as e:
                marketingData_utmMedium = ''
            try:
                marketingData_utmCampaign = compra['marketingData']['utmCampaign']
            except (IndexError, TypeError, KeyError) as e:
                marketingData_utmCampaign = ''

            # shippingData
            shippingData = compra['shippingData']

            try:
                shippingData_address = shippingData['address']
                shippingData_address_country = shippingData_address['country']
                shippingData_address_state = shippingData_address['state']
                shippingData_address_city = shippingData_address['city']
                shippingData_address_street = shippingData_address['street']
                shippingData_address_postalCode = shippingData_address['postalCode']
            except (IndexError, TypeError, KeyError) as e:
                shippingData_address_country = ''
                shippingData_address_state = ''
                shippingData_address_city = ''
                shippingData_address_street = ''
                shippingData_address_postalCode = ''

            shippingData_logisticsInfo = compra['shippingData']['logisticsInfo']
            logisticsInfo = []
            for shippingData in shippingData_logisticsInfo:
                logisticInfo = {}
                logisticInfo['shippingData_logisticsInfo_itemIndex'] = shippingData['itemIndex']
                logisticInfo['shippingData_logisticsInfo_price'] = shippingData['price']/100
                logisticInfo['shippingData_logisticsInfo_listPrice'] = shippingData['listPrice']/100
                logisticInfo['shippingData_logisticsInfo_shippingEstimate'] = shippingData['shippingEstimate']

                logisticInfo['shippingData_logisticsInfo_shippingEstimateDate'] = shippingData['shippingEstimateDate']
                if(shippingData['shippingEstimateDate'] != None):
                    logisticInfo['shippingData_logisticsInfo_shippingEstimateDate'] = logisticInfo[
                        'shippingData_logisticsInfo_shippingEstimateDate']
                    # # substring da data e hora
                    # logisticInfo['shippingData_logisticsInfo_shippingEstimateDate'] = logisticInfo[
                    #     'shippingData_logisticsInfo_shippingEstimateDate'][:19]
                    # logisticInfo['shippingData_logisticsInfo_shippingEstimateDate'] = datetime.datetime.strptime(
                    #     logisticInfo['shippingData_logisticsInfo_shippingEstimateDate'], '%Y-%m-%dT%H:%M:%S')  # converter para datetime
                    # # alterar tzinfo de None para utc
                    # logisticInfo['shippingData_logisticsInfo_shippingEstimateDate'] = logisticInfo[
                    #     'shippingData_logisticsInfo_shippingEstimateDate'].replace(tzinfo=pytz.utc)
                    # # alterar para o timezone do Brasil
                    # logisticInfo['shippingData_logisticsInfo_shippingEstimateDate'] = logisticInfo[
                    #     'shippingData_logisticsInfo_shippingEstimateDate'].astimezone(brtz)
                    # logisticInfo['shippingData_logisticsInfo_shippingEstimateDate'] = logisticInfo[
                    #     'shippingData_logisticsInfo_shippingEstimateDate'].strftime('%d/%m/%Y %H:%M:%S')

                logisticInfo['shippingData_logisticsInfo_selectedSla'] = shippingData['selectedSla']
                logisticInfo['shippingData_logisticsInfo_deliveryCompany'] = shippingData['deliveryCompany']
                logisticsInfo.append(logisticInfo)

            # paymentData
            paymentData = compra['paymentData']

            paymentData_giftCards_value = 0
            paymentData_giftCards_balance = 0

            if(len(paymentData['giftCards']) > 0):
                for giftCards in paymentData['giftCards']:
                    paymentData_giftCards_value = paymentData_giftCards_value + \
                        giftCards['value']
                    paymentData_giftCards_balance = paymentData_giftCards_value + \
                        giftCards['balance']

            transactions = paymentData['transactions']
            paymentData_payments_id = '-'
            paymentData_payments_paymentSystemName = '-'
            paymentData_payments_value = '-'
            paymentData_payments_installments = '-'
            if(len(transactions[0]['payments']) > 0):
                for payment in transactions[0]['payments']:
                    paymentData_payments_id = paymentData_payments_id + ' | ' + \
                        payment['id'] if paymentData_payments_id != '-' else payment['id']
                    paymentData_payments_paymentSystemName = paymentData_payments_paymentSystemName + ', ' + \
                        payment['paymentSystemName'] if paymentData_payments_paymentSystemName != '-' else payment['paymentSystemName']
                    paymentData_payments_value = paymentData_payments_value + ' | ' + \
                        str(locale.currency(payment['value']/100, grouping=True, symbol=None),) if paymentData_payments_value != '-' else str(
                            locale.currency(payment['value']/100, grouping=True, symbol=None),)
                    paymentData_payments_installments = payment['installments']

            # Seta as variáveis na classe
            for idx in range(len(item_compra)):
                r = Orders(
                    orderId,
                    origin,
                    affiliateId,
                    salesChannel,
                    status,
                    statusDescription,
                    creationDateTime,
                    lastChangeDateTime,
                    authorizedDateTime,
                    invoicedDateTime,
                    total_Itens,
                    total_Discounts,
                    total_Shipping,
                    total_Tax,

                    item_compra[idx]['items_id'],
                    item_compra[idx]['items_quantity'],
                    item_compra[idx]['items_price'],
                    item_compra[idx]['items_listPrice'],
                    item_compra[idx]['items_priceTags_value'],
                    item_compra[idx]['items_additionalInfo_brandName'],

                    item_compra[idx]['changesAttachment_itemsAdded_reason'],
                    item_compra[idx]['changesAttachment_itemsAdded_price'],
                    item_compra[idx]['changesAttachment_itemsAdded_quantity'],
                    item_compra[idx]['changesAttachment_itemsRemoved_reason'],
                    item_compra[idx]['changesAttachment_itemsRemoved_price'],
                    item_compra[idx]['changesAttachment_itemsRemoved_quantity'],

                    marketingData_utmSource,
                    marketingData_utmMedium,
                    marketingData_utmCampaign,

                    clientProfileData_firstName,
                    clientProfileData_phone,
                    clientProfileData_email,

                    shippingData_address_country,
                    shippingData_address_state,
                    shippingData_address_city,
                    shippingData_address_street,
                    shippingData_address_postalCode,

                    logisticsInfo[idx]['shippingData_logisticsInfo_itemIndex'],
                    logisticsInfo[idx]['shippingData_logisticsInfo_price'],
                    logisticsInfo[idx]['shippingData_logisticsInfo_shippingEstimate'],
                    logisticsInfo[idx]['shippingData_logisticsInfo_shippingEstimateDate'],
                    logisticsInfo[idx]['shippingData_logisticsInfo_listPrice'],
                    logisticsInfo[idx]['shippingData_logisticsInfo_selectedSla'],
                    logisticsInfo[idx]['shippingData_logisticsInfo_deliveryCompany'],

                    paymentData_giftCards_value,
                    paymentData_giftCards_balance,
                    paymentData_payments_id,
                    paymentData_payments_paymentSystemName,
                    paymentData_payments_value,
                    paymentData_payments_installments
                )

                # insere o objeto no array
                resultado.append(r)

    #  EXPORTAR PARA CSV
    # seta as colunas do data frame
    df = pandas.DataFrame(columns=[
        'CD_ORDER', 'EXTRACTION_DATE',
        'ORIGIN', 'AFFILIATE_ID', 'SALES_CHANNEL', 'STATUS', 'STATUS_DESCRIPTION', 'DT_CREATION',
        'DT_LAST_CHANGE', 'DT_AUTORIZED', 'DT_INVOICED', 'TOTALS_ITEMS', 'TOTALS_DISCOUNTS', 'TOTALS_SHIPPING',
        'TOTALS_TAX', 'ITEMS_ID', 'ITEMS_QUANTITY', 'ITEMS_PRICE', 'ITEMS_LIST_PRICE', 'ITEMS_PRICETAG_VALUE',
        'ITEMS_ADDITIONALINFO_BRANDNAME', 'ITEMS_ADDED_REASON', 'ITEMS_ADDED_PRICE', 'ITEMS_ADDED_QUANTITY',
        'ITEMS_REMOVED_REASON', 'ITEMS_REMOVED_PRICE', 'ITEMS_REMOVED_QUANTITY', 'MARKETINGDATA_UTMSOURCE',
        'MARKETINGDATA_UTMMEDIUM', 'MARKETINGDATA_UTMCAMPAIGN', 'SHIPPINGDATA_ADDRESS_COUNTRY', 'SHIPPINGDATA_ADDRESS_STATE',
        'SHIPPINGDATA_ADDRESS_CITY', 'SHIPPINGDATA_ADDRESS_STREET', 'SHIPPINGDATA_POSTAL_CODE', 'SHIPPINGDATA_LOGISTICSINFO_ITEM_INDEX',
        'SHIPPINGDATA_LOGISTICSINFO_PRICE', 'SHIPPINGDATA_LOGISTICSINFO_SHIPPINGESTIMATE',
        'SHIPPINGDATA_LOGISTICSINFO_SHIPPINGESTIMATEDATE', 'SHIPPINGDATA_LOGISTICSINFO_LISTPRICE',
        'SHIPPINGDATA_LOGISTICSINFO_SELECTEDSLA', 'SHIPPINGDATA_LOGISTICSINFO_DELIVERYCOMPANY',
        'PAYMENTDATA_GIFTCARDS_VALUE', 'PAYMENTDATA_GIFTCARDS_BALANCE', 'PAYMENTDATA_PAYMENTS_ID',
        'PAYMENTDATA_PAYMENTS_PAYMENTSYSTEMNAME', 'PAYMENTDATA_PAYMENTS_VALUE', 'PAYMENTDATA_PAYMENTS_INSTALLMENT'
    ])
    # itera no resultado para atribuir as tuplas em cada linha do dataframe
    for i in range(len(resultado)):
        df.loc[len(df)] = [
            resultado[i].orderId[0],
            resultado[i].extractionDate,
            resultado[i].origin[0],
            resultado[i].affiliateId[0],
            resultado[i].salesChannel[0],
            resultado[i].status[0],
            resultado[i].statusDescription[0],
            resultado[i].creationDateTime[0],
            resultado[i].lastChangeDateTime[0],
            resultado[i].authorizedDateTime[0],
            resultado[i].invoicedDateTime[0],
            # locale.currency(resultado[i].total_Itens[0], grouping=True, symbol=None),
            # locale.currency(resultado[i].total_Discounts[0], grouping=True, symbol=None),
            # locale.currency(resultado[i].total_Shipping[0], grouping=True, symbol=None),
            # locale.currency(resultado[i].total_Tax[0], grouping=True, symbol=None),
            resultado[i].total_Itens[0],
            resultado[i].total_Discounts[0],
            resultado[i].total_Shipping[0],
            resultado[i].total_Tax[0],
            resultado[i].items_id[0],
            resultado[i].items_quantity[0],
            # locale.currency(resultado[i].items_price[0], grouping=True, symbol=None),
            # locale.currency(resultado[i].items_listPrice[0], grouping=True, symbol=None),
            # locale.currency(resultado[i].items_priceTags_value[0], grouping=True, symbol=None),
            resultado[i].items_price[0],
            resultado[i].items_listPrice[0],
            resultado[i].items_priceTags_value[0],
            resultado[i].items_additionalInfo_brandName[0],
            resultado[i].changesAttachment_itemsAdded_reason[0],
            # locale.currency(int(resultado[i].changesAttachment_itemsAdded_price[0] or 0), grouping=True, symbol=None),
            int(resultado[i].changesAttachment_itemsAdded_price[0] or 0),
            int(resultado[i].changesAttachment_itemsAdded_quantity[0] or 0),
            resultado[i].changesAttachment_itemsRemoved_reason[0],
            # locale.currency(int(resultado[i].changesAttachment_itemsRemoved_price[0] or 0), grouping=True, symbol=None),
            int(resultado[i].changesAttachment_itemsRemoved_price[0] or 0),
            int(resultado[i].changesAttachment_itemsRemoved_quantity[0] or 0),
            resultado[i].marketingData_utmSource[0],
            resultado[i].marketingData_utmMedium[0],
            resultado[i].marketingData_utmCampaign[0],
            resultado[i].shippingData_address_country[0],
            resultado[i].shippingData_address_state[0],
            resultado[i].shippingData_address_city[0],
            resultado[i].shippingData_address_street[0],
            resultado[i].shippingData_address_postalCode[0],
            resultado[i].shippingData_logisticsInfo_itemIndex[0],
            # locale.currency(resultado[i].shippingData_logisticsInfo_price[0], grouping=True, symbol=None),
            resultado[i].shippingData_logisticsInfo_price[0],
            resultado[i].shippingData_logisticsInfo_shippingEstimate[0],
            resultado[i].shippingData_logisticsInfo_shippingEstimateDate[0],
            # locale.currency(resultado[i].shippingData_logisticsInfo_listPrice[0], grouping=True, symbol=None),
            resultado[i].shippingData_logisticsInfo_listPrice[0],
            resultado[i].shippingData_logisticsInfo_selectedSla[0],
            resultado[i].shippingData_logisticsInfo_deliveryCompany[0],
            # locale.currency(resultado[i].paymentData_giftCards_value[0]/100, grouping=True, symbol=None),
            # locale.currency(resultado[i].paymentData_giftCards_balance[0]/100, grouping=True, symbol=None),
            int(resultado[i].paymentData_giftCards_value[0]/100 or 0),
            int(resultado[i].paymentData_giftCards_balance[0]/100 or 0),
            resultado[i].paymentData_payments_id[0],
            resultado[i].paymentData_payments_paymentSystemName[0],
            resultado[i].paymentData_payments_value[0],
            resultado[i].paymentData_payments_installments[0],
        ]

    # salva o resultado do dataframe no banco
    # ot.execute_query(conn, 'INSERT * ')
    df.to_sql(con=engine, name='ORDER_VTEX', if_exists='append', index=False)

    # # salva o resultado do dataframe em um arquivo csv
    # df.to_csv(f".\Arquivos\order_Vtex{dt}-{x}.csv",
    #           index=False, encoding='utf-8')
    # tsFim = datetime.datetime.now()

    print(f'Arquivo criado: order_Vtex{dt}-{x}.csv')
